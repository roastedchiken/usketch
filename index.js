$(document).ready(function(){

	var socket = io.connect();
	var MyRoomId = null;
	var canvas = document.getElementById('sketchpad');
	var ctx = canvas.getContext("2d");
	var mouse = {x: 0, y: 0};
	var mouseDown = false;

	$('.container-fluid').hide();
	$('#leave-room-button').data('roomId', null);
	$('#leave-room-button').hide();

	$('#room-list').on('click', '.room', function(e){
		var oldRoomId = $('#leave-room-button').data('roomId');
		if (oldRoomId != null) {
			socket.emit('leaveRoom', oldRoomId);
			$('#leave-room-button').data('roomId', null);
		}
		var roomId = e.target.id;
		console.log('joining room:' + roomId);
		socket.emit('joinRoom', roomId);
		$('#umessage').focus();
	});
	$('#leave-room-button').click(function(){
		roomId = $(this).data('roomId');
		if (roomId != null) {
			console.log('leave room button data: ' + roomId);
			socket.emit('leaveRoom', roomId);
			$(this).data('roomId', null)
			$(this).hide();
			$('#create-room-form-button').show();
			$('.current-room').text('Lobby');
			$('.users-in-room').text('Welcome back to the Lobby');
		}
	});
	$('#join-lobby').click(function(){
		var uname = $('#uname').val();
		if (uname != '' && uname.length < 26) {
			socket.emit('join', uname);
			//TODO loading screen here
			$('.jumbotron').hide();	// Maybe just change the text?
			$('.container-fluid').show();
			// $('#umessage').focus();
		} else {
			alert('Please enter a valid username');
			// $('#uname').addClass('has-error');
			// $('#uname').parent().prepend('<label class="control-label" for="uname">Input with error</label>');
			//TODO change this to error the form instead
		}
		return false;
	});
	$('#create-room-form-button').click(function(){
		$('#create-room-form').toggle();
		$('#room-form-name').focus();
	});
	$('#create-room-button').click(function() {
		var roomName = $('#room-form-name').val();
		if (roomName !="" && name.length < 26) {
			socket.emit('createRoom', roomName);
			$('#room-form-name').val('');
			$(this).parent().hide();
		}
		return false;
	});
	$('#send-message').submit(function() {
		var msg = $('#umessage').val();
		if (msg != null) {
			socket.emit('send', msg );
			console.log('sending message');
			$('#umessage').val('');
		}
		return false;
	});

	// Automatic scrolldown when user has not scrolled back up found here:
	// http://stackoverflow.com/questions/25505778/automatically-scroll-down-chat-div
	socket.on('update', function(msg){
		var messages = document.getElementById("messages");
		var shouldScroll = messages.scrollTop + messages.clientHeight === messages.scrollHeight;
		$('.chat-cell').append('<p>' + msg + '<p>');
		if (shouldScroll) {
			messages.scrollTop = messages.scrollHeight;
		}
	});

	socket.on('update-users', function(data) {
		users = data.users;
		console.log('updated users list');
	});

	socket.on('roomList', function(data) {
		var roomLen = Object.keys(data.rooms).length;
		$('#rooms').text('Rooms: ' + roomLen);
		if (roomLen > 0) {
			$('#room-container').empty();
			$.each(data.rooms, function(id, room) {
				var currentRoomId = $('#leave-room-button').data('roomId');
				if (currentRoomId != null) {
					if (id == currentRoomId) {
						var html = '<button id='+ id +' class="current-room list-group-item" >' + room.name + '</button>';
						$('#room-container').append(html);
					} else {
						var html = '<button id='+ id +' class="room list-group-item" >' + room.name + '</button>';
						$('#room-container').append(html);	
					}
				} else {
					var html = '<button id='+ id +' class="room list-group-item" >' + room.name + '</button>';
					$('#room-container').append(html);	
				}
				
			})
		} else {
			$('#room-container').empty();
			var noRooms = '<p id="noRooms" class="list-group-item">There are no rooms yet :(</p>';
			$('#room-container').append(noRooms);
		}
		console.log('update rooms list');
	});

	socket.on('sendRoomId', function(data) {
		var roomId = data.id;
		var room = $('#' + roomId);
		room.removeClass('room');
		room.addClass('current-room');
		room.addClass('active');
		room.addClass('no-pointer');
		$('#create-room-form-button').hide();
		$('#leave-room-button').data('roomId', roomId);
		console.log('setting data for leave room button');
		$('#leave-room-button').show();
		$('.current-room').text(room.text());
		$('.users-in-room').text('Users: ');
	});

	socket.on('roomClosed', function() {
		$('#leave-room-button').data('roomId', null);
		$('#leave-room-button').hide();
		$('#create-room-form-button').show();
		$('.current-room').text('Lobby');
		$('.users-in-room').text('Welcome back to the Lobby');
	});

	// TODO extract this into a module 
	// Canvas stuff
	canvas.addEventListener('mousemove', function(e) {
	  mouse.x = e.pageX - this.offsetLeft;
	  mouse.y = e.pageY - this.offsetTop - 143; //143 is height of header. Will try to change this later to
	                                            // remove need for hardcoded length 
	}, false);

	ctx.lineWidth = 3;
	ctx.lineJoin = 'round';
	ctx.lineCap = 'round';
	ctx.strokeStyle = '#00CC99';
	 
	socket.on('startStroke', function(point){
	    startPaint(point);
	    // console.log('doing startstroke');
	});

	socket.on('onStroke', function(point){
	    onPaint(point);
	    // console.log('doing onstroke');
	});
	
	socket.on('colorChange', function(color){
	    ctx.strokeStyle = color;
	});
	// From: http://stackoverflow.com/questions/2142535/how-to-clear-the-canvas-for-redrawing
	socket.on('clearCanvas', function() {
		ctx.clearRect(0, 0, canvas.width, canvas.height);
	});

	canvas.addEventListener('mousedown', function() {
	    mouseDown = true;
	    startPaint(mouse);
	    socket.emit('startStroke', mouse);
	}, false);

	canvas.addEventListener('mousemove', function() {
	    if (!mouseDown) return;
	    onPaint(mouse);
	    socket.emit('onStroke', mouse);
	}, false);
	 
	canvas.addEventListener('mouseup', function() {
	    mouseDown = false;
	}, false);
	 
	var startPaint = function(pos) {
	    ctx.beginPath();
	    ctx.moveTo(pos.x - 0.4, pos.y - 0.4);
	    ctx.lineTo(pos.x, pos.y);
	    ctx.stroke();
	};

	var onPaint = function(pos) {
	    ctx.lineTo(pos.x, pos.y);
	    ctx.stroke();    
	};

	// $("#purple").click(function() {
	// 	ctx.strokeStyle = "#cb3594";
	//     socket.emit('colorChange', ctx.strokeStyle);
	// });

});