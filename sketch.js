$(document).ready(function() {

    var canvas = document.getElementById('sketchpad');
    var ctx = canvas.getContext('2d');
    var mouse = {x: 0, y: 0};
    var mouseDown = false;

    canvas.addEventListener('mousemove', function(e) {
      mouse.x = e.pageX - this.offsetLeft;
      mouse.y = e.pageY - this.offsetTop - 143; //143 is height of header. Will try to change this later to
                                                // remove need for hardcoded length 
    }, false);

    ctx.lineWidth = 3;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.strokeStyle = '#00CC99';
     
    socket.on('startStroke', function(point){
        startPaint(point);
        // console.log('doing startstroke');
    });

    socket.on('onStroke', function(point){
        onPaint(point);
        // console.log('doing onstroke');
    });

    socket.on('colorChange', function(color){
        ctx.strokeStyle = color;
    });

    canvas.addEventListener('mousedown', function() {
        mouseDown = true;
        startPaint(mouse);
        socket.emit('startStroke', mouse);
    }, false);

    canvas.addEventListener('mousemove', function() {
        if (!mouseDown) return;
        onPaint(mouse);
        socket.emit('onStroke', mouse);
    }, false);
     
    canvas.addEventListener('mouseup', function() {
        mouseDown = false;
    }, false);
     
    var startPaint = function(pos) {
        ctx.beginPath();
        ctx.moveTo(pos.x - 0.4, pos.y - 0.4);
        ctx.lineTo(pos.x, pos.y);
        ctx.stroke();
    };

    var onPaint = function(pos) {
        ctx.lineTo(pos.x, pos.y);
        ctx.stroke();    
    };

    // $('#purple').click(function() {
    // 	ctx.strokeStyle = '#cb3594';
    //     socket.emit('colorChange', ctx.strokeStyle);
// });

});