An online pictionary game that may expand to be more. Inspiration taken from:
isketch.net
http://www.tamas.io/advanced-chat-using-node-js-and-socket-io-episode-1/
http://buildnewgames.com/real-time-multiplayer/
https://www.pubnub.com/blog/2014-09-03-multiuser-draw-html5-canvas-tutorial/

To run the server:
* you'll need to make sure all dependencies are installed
	--socket.io
	-- node-uuid
	-- express
then sure run the server
$ node myserver.js
You can now connect to the server by going to 127.0.0.1:4004.
If you wish to change the port you can do so in myserver.js and change the gameport variable on line 2

TODO:
-Features:
* unique names and roomnames
* private rooms
* Drawing tools (colors, box, triangle, circle, spray can, fill, eraser)
* Actual game (timer, word list, text input for guesses, checkmark for who draws)
* Chat and canvas history (for when people join mid game)
* Display users in rooms
* Have the Room creation div not scroll when there are a lot of rooms
* Whisper feature?
-Refactor:
* have CSS be logical (general to specific top down)
* Include the header in a row so the 143 px adjustments aren't necessary
* Extract Canvas stuff into JS module
* look into if we need client.room as well as users[client.id].room and if there's anyway to get the data
  using the built in socket features.
* remove test output