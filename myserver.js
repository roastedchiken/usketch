var
    gameport = process.env.PORT || 4004,

    io = require('socket.io'),
    express = require('express'),
    UUID = require('node-uuid'),
    Room = require('./room.js'),

    users = {},
    rooms = {},
    clients = [],

    verbose = false,
    http = require('http'),
    app = express(),
    server = http.createServer(app);


server.listen( gameport );

console.log('\t :: Express :: Listening on port ' + gameport );

app.get( '/', function( req, res) {
    res.sendFile(__dirname + '/index.html' );
});

app.get( '/*', function( req, res, next ) {
    var file = req.params[0];

    if(verbose) console.log('\t :: Express :: file requested : ' + file);

    res.sendFile(__dirname + '/' + file);
});


var sio = io.listen(server);
var users = {};
var current_drawer = null;

// game_server = require('./game.server.js')

sio.sockets.on('connection', function (client) {
    client.on('join', function(username){
        roomId = null;
        users[client.id] = {'username' : username, 'room' : roomId};
        sio.sockets.emit('update-users', users);
        client.emit('roomList', { rooms: rooms });
        clients.push(client);
        client.join('lobby');
        sio.sockets.in('lobby').emit('update', users[client.id].username + ' has joined the lobby');
    });           

    client.on('createRoom', function(roomname) {
        // One room per client to avoid abuse. Delete room on leaving?
        if (users[client.id].room === null) {
            console.log('roomname: ' + roomname);
            var roomid = UUID();
            var room = new Room(roomname, roomid, client.id);
            rooms[roomid] = room;                               // enter room object with id as key in dictionary of rooms
            sio.sockets.emit('roomList', {rooms: rooms});   // update room list
            client.room = roomname;                          // update client's room attribute with the room
            client.leave('lobby');
            sio.sockets.in('lobby').emit('update', users[client.id].username + ' has left the lobby');
            client.join(client.room);                       // auto-join created room
            client.emit('update', users[client.id].username + ' has created the room');
            client.emit('update', 'Welcome to ' + roomname);
            room.addUser(client.id);                      // room keeps track of users in room
            users[client.id].room = roomid;                     // update the room attribute for the user in the dictionary
            client.emit('sendRoomId', {id: roomid});
            client.emit('clearCanvas');
        } else {
            client.emit('update', 'You have already created a room' )
        }
    });

    client.on('joinRoom', function(id) {
        console.log('received joinRoom request');
        if (!(id in rooms)) {
            client.emit('update', 'something went wrong.. (Invalid Room ID)');
        } else {
            var room = rooms[id];
            if(client.id === room.owner) {
                client.emit('update', 'You are the owner of this room');
            } else {
                if (users[client.id].room === id) {
                    client.emit('update', 'You are already in this room');
                } else {
                    client.leave('lobby');
                    room.addUser(client.id);
                    users[client.id].room = id;
                    client.room = room.name;
                    client.join(client.room);
                    user = users[client.id];
                    sio.sockets.in(client.room).emit('update', users[client.id].username + ' has joined the room');
                    client.emit('update', 'Welcome to ' + room.name);
                    client.emit('sendRoomId', {id: id});
                    client.emit('clearCanvas');
                }
            }
        }
    });

    client.on('send', function(msg) {
        //TODO Investigate if this works fully
        var msgToSend = users[client.id].username +': ' + msg;
        if (client.room != null) {
            console.log('message to room' + client.room);
            sio.sockets.in(client.room).emit('update', msgToSend);
        } else {
            //TODO lobby chat
            console.log('message to lobby: ' + msg);
            sio.sockets.in('lobby').emit('update', msgToSend);
        }
    });

    client.on('leaveRoom', function(id) {
        if (!(id in rooms)) {
            client.emit('update', 'something went wrong.. (Invalid Room ID)');
        } else {
            console.log('leaving room');
            var room = rooms[id];
            if (client.id === room.owner) {
                var i = 0;
                client.leave(room.name);
                sio.sockets.in(client.room).emit('update', 'the owner has left the room');
                client.room = null;
                // TODO better way to boot?
                while(i < clients.length) {
                    if(room.containsUser(clients[i].id)) {
                        console.log('booting client' + users[clients[i].id].username);
                        users[clients[i].id].room = null;
                        clients[i].room = null;
                        clients[i].leave(room.name);
                        clients[i].emit('roomClosed', room.name);
                        clients[i].emit('clearCanvas');
                        clients[i].join('lobby');
                        sio.sockets.in('lobby').emit('update', users[clients[i].id].username + ' has joined the lobby');
                    }
                    i++;
                }
                delete rooms[id];
                sio.sockets.emit('roomList', {rooms: rooms});
                
            } else {
                if (users[client.id].room === id) {
                    room.removeUser(client.id);
                    sio.sockets.emit('update', users[client.id].username + ' has left the room');
                    client.leave(room.name);
                    client.room = null;
                }
            }
            users[client.id].room = null;
            client.room = null;
            client.emit('clearCanvas');
            client.join('lobby');
            sio.sockets.in('lobby').emit('update', users[client.id].username + ' has joined the lobby');
        }
    });

    // Not currently supported
    // client.on('removeRoom', function(id){
    //     var room = rooms[id];
    //     if (room) {
    //         if (client.id === room.owner) {
    //             sio.sockets.in(client.room).emit('update', 'the owner is closing the room');
    //             var i = 0;
    //             //TODO better way for htis? maybe make it a function too...
    //             while (i < clients.length) {
    //                 if (clients[i].id === room.users[i]) {
    //                     users[clients[i].id].room = null;
    //                     clients[i].leave(room.name);
    //                 }
    //                 i++;
    //             }
    //             delete rooms[id];
    //             sio.sockets.emit('roomList', {rooms: rooms});
    //         } else {
    //             client.emit('update', 'Only the owner can remove the room');
    //         }
    //     }
    // })

    client.on('startStroke', function(point){
        var room = 'lobby';
        if (rooms[users[client.id].room] != null) {
            room = rooms[users[client.id].room].name;
        }
        client.broadcast.to(room).emit('startStroke', point);
        // console.log('Server received startStroke');
    });
    client.on('onStroke', function(point){
        var room = 'lobby';
        if (rooms[users[client.id].room] != null) {
            room = rooms[users[client.id].room].name;
        }
        client.broadcast.to(room).emit('onStroke', point);
        // console.log('Server received onStroke');
    });
    client.on('colorChange', function(color){
        var room = 'lobby';
        if (rooms[users[client.id].room] != null) {
            room = rooms[users[client.id].room].name;
        }
        client.broadcast.to(room).emit('colorChange', color);
    });

    client.on('disconnect', function() {
        console.log('client disconnecting');
        if (users[client.id]) {
            var user = users[client.id];
            // User not in room
            if(user.room === null) {
                sio.sockets.in('lobby').emit('update', users[client.id].username + ' has disconnected');
                delete users[client.id];
                sio.sockets.emit('update-users', users);
            } else {
                console.log('Disconnected is room owner');
                //TODO maybe too trusting that user is in a room that exists
                var roomId = users[client.id].room;
                var room = rooms[roomId];
                // If disconnect is room owner, boot and delete room
                if (client.id === room.owner) {
                    var i = 0;
                    // TODO better way to boot everyone from room?
                    sio.sockets.in(client.room).emit('update', 'the owner has left the room');
                    while( i < clients.length) {
                        if (room.containsUser(clients[i].id)) {
                            users[clients[i].id].room = null;
                            clients[i].room = null;
                            clients[i].leave(room.name);
                            clients[i].emit('roomClosed', room.name);
                            clients[i].join('lobby');
                            sio.sockets.in('lobby').emit('update', clients[i].username + ' has joined the lobby');
                        }
                        ++i
                    }
                    delete rooms[roomId];
                // Otherwise just remove from room
                } else {
                    room.removeUser(client.id);
                    sio.sockets.in(client.room).emit('update', users[client.id].username + ' has disconnected')
                }
                delete users[client.id];

            }
        }
    });
});

function userLeaveRoom(user, room) {

};

function ownerLeaveRoom(user, room) {

};

function userJoinRoom(user, room) {

};