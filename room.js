function Room(name, id, owner) {  
  this.name = name;
  this.id = id;
  this.owner = owner;
  this.users = [];
  this.status = 'available';
};

Room.prototype.addUser = function(userID) {  
  if (this.status === 'available') {
    this.users.push(userID);
  }
};

Room.prototype.removeUser = function(userID) {  
  var index = this.users.indexOf(userID);
  if (index > -1) {
  	this.users.splice(index, 1);
  }
};

Room.prototype.containsUser = function(userId) {
  for (var i =0; i< this.users.length; i++)   {
    if (this.users[i] == userId) {
      return true;
    }
  }
  return false;
}

module.exports = Room; 